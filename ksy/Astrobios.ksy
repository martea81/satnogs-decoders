---
meta:
  id: astrobios
  file-extension: astrobios
  endian: le
doc: |
  :field init: ax25_frame.ax25_header.init
  :field callsign: ax25_frame.ax25_header.src_callsign.callsign
  :field dest_callsign_callsign: ax25_frame.ax25_header.dest_callsign.callsign
  :field ctrl_field: ax25_frame.ax25_header.ctrl_field
  :field pid: ax25_frame.ax25_header.pid
  :field beacon_header: ax25_frame.ax25_data_field.beacon_header
  :field radio_idx: ax25_frame.ax25_data_field.radio_idx
  :field radio_ack_index: ax25_frame.ax25_data_field.radio_ack_index
  :field type: ax25_frame.ax25_data_field.type
  :field size: ax25_frame.ax25_data_field.size
  :field sub_type: ax25_frame.ax25_data_field.sub_type
  :field total_mission_minutes: ax25_frame.ax25_data_field.total_mission_minutes
  :field status: ax25_frame.ax25_data_field.status
  :field mission_phase: ax25_frame.ax25_data_field.mission_phase
  :field time_now: ax25_frame.ax25_data_field.time_now
  :field eps_battery_voltage: ax25_frame.ax25_data_field.eps_battery_voltage
  :field eps_battery_voltage_max: ax25_frame.ax25_data_field.eps_battery_voltage_max
  :field eps_battery_voltage_min: ax25_frame.ax25_data_field.eps_battery_voltage_min
  :field eps_battery_discharge_current: ax25_frame.ax25_data_field.eps_battery_discharge_current
  :field eps_battery_discharge_current_max: ax25_frame.ax25_data_field.eps_battery_discharge_current_max
  :field eps_battery_discharge_current_min: ax25_frame.ax25_data_field.eps_battery_discharge_current_min
  :field eps_battery_charge_current: ax25_frame.ax25_data_field.eps_battery_charge_current
  :field eps_battery_charge_current_max: ax25_frame.ax25_data_field.eps_battery_charge_current_max
  :field eps_battery_charge_current_min: ax25_frame.ax25_data_field.eps_battery_charge_current_min
  :field eps_battery_temperature: ax25_frame.ax25_data_field.eps_battery_temperature
  :field eps_battery_temperature_max: ax25_frame.ax25_data_field.eps_battery_temperature_max
  :field eps_battery_temperature_min: ax25_frame.ax25_data_field.eps_battery_temperature_min
  :field eps_pv0_current: ax25_frame.ax25_data_field.eps_pv0_current
  :field eps_pv0_current_max: ax25_frame.ax25_data_field.eps_pv0_current_max
  :field eps_pv0_current_min: ax25_frame.ax25_data_field.eps_pv0_current_min
  :field eps_pv1_current: ax25_frame.ax25_data_field.eps_pv1_current
  :field eps_pv1_current_max: ax25_frame.ax25_data_field.eps_pv1_current_max
  :field eps_pv1_current_min: ax25_frame.ax25_data_field.eps_pv1_current_min
  :field eps_3v3_current: ax25_frame.ax25_data_field.eps_3v3_current
  :field eps_3v3_current_max: ax25_frame.ax25_data_field.eps_3v3_current_max
  :field eps_3v3_current_min: ax25_frame.ax25_data_field.eps_3v3_current_min
  :field eps_5v_current: ax25_frame.ax25_data_field.eps_5v_current
  :field eps_5v_current_max: ax25_frame.ax25_data_field.eps_5v_current_max
  :field eps_5v_current_min: ax25_frame.ax25_data_field.eps_5v_current_min
  :field eps_pbus_current: ax25_frame.ax25_data_field.eps_pbus_current
  :field eps_pbus_current_max: ax25_frame.ax25_data_field.eps_pbus_current_max
  :field eps_pbus_current_min: ax25_frame.ax25_data_field.eps_pbus_current_min
  :field eps_active_sensor: ax25_frame.ax25_data_field.eps_active_sensor
  :field pl2_start_free_address: ax25_frame.ax25_data_field.pl2_start_free_address
  :field pl2_end_free_address: ax25_frame.ax25_data_field.pl2_end_free_address
  :field pl2_active_flag_status: ax25_frame.ax25_data_field.pl2_active_flag_status
  :field pl2_mcu_protected_counts: ax25_frame.ax25_data_field.pl2_mcu_protected_counts
  :field pl2_mcu_external_counts: ax25_frame.ax25_data_field.pl2_mcu_external_counts
  :field pl2_imu_protected_counts: ax25_frame.ax25_data_field.pl2_imu_protected_counts
  :field pl2_imu_protected_interval: ax25_frame.ax25_data_field.pl2_imu_protected_interval
  :field pl2_imu_external_counts: ax25_frame.ax25_data_field.pl2_imu_external_counts
  :field pl2_imu_external_interval: ax25_frame.ax25_data_field.pl2_imu_external_interval
  :field amature_packets: ax25_frame.ax25_data_field.amature_packets
  :field amature_tx_packets: ax25_frame.ax25_data_field.amature_tx_packets
  :field last_time_radio_ham_rx: ax25_frame.ax25_data_field.last_time_radio_ham_rx
  :field last_time_radio_ham_tx: ax25_frame.ax25_data_field.last_time_radio_ham_tx
  :field radio_ham_call_sign: ax25_frame.ax25_data_field.radio_ham_call_sign
  :field event_counter: ax25_frame.ax25_data_field.event_counter
  :field pl2_last_beacon_address: ax25_frame.ax25_data_field.pl2_last_beacon_address
  :field pl2_ecperiment_data: ax25_frame.ax25_data_field.pl2_ecperiment_data
  :field crc: ax25_frame.ax25_data_field.crc

seq:
  - id: ax25_frame
    type: ax25_frame
    doc-ref: 'https://www.researchgate.net/figure/AX25-data-frame-in-KISS-mode-for-the-requested-file-upload-command_fig3_320035650'
    
types:
  ax25_frame:
    seq:
      - id: ax25_header
        type: ax25_header
      - id: ax25_data_field
        type:
          switch-on: _io.size #ax25_header.ctl & 0x13 # CAPIRE COME DISTINGUERE TRA MCU, FPGA E RADIO
          cases:
            235: beacon_0
            235: beacon_1
            235: beacon_2

  ax25_header:
    seq:
      - id: init
        type: u1
      - id: src_callsign
        type: callsign
      - id: dest_callsign
        type: callsign
      - id: ctrl_field
        type: u1
      - id: pid
        type: u2
  
  callsign:
    seq:
      - id: callsign
        process: ror(1)
        size: 7
        
  beacon_0:
   seq:
      - id: beacon_header
        type: str
        encoding: ASCII
        size: 3
      - id: radio_idx
        type: u2
      - id: radio_ack_index
        type: u2
      - id: type
        type: u1
      - id: size
        type: u1
      - id: sub_type
        type: u1
      - id: total_mission_minutes
        type: u4
      - id: status
        type: u1
      - id: mission_phase
        type: u1
      - id: last_mission_phase_minute_ref
        type: u4
      - id: memory_boot_type
        type: u1
      - id: reboots_counter
        type: u2
      - id: dont_init_memory
        type: u1
      - id: bus_sensors_status
        type: u1
      - id: debug_in_on
        type: u1
      - id: ignore_crc
        type: u1
      - id: internal_rtc
        type: u4
      - id: time_at_boot
        type: u4
      - id: pl1_exp_protocol_setup
        type: u1
      - id: pl1_exp_boot_type
        type: u1
      - id: pl1_exp_running
        type: u1
      - id: pl1_exp_status_mode
        type: u1
      - id: pl1_current_exp
        type: u1
      - id: pl1_current_step
        type: u4
      - id: pl1_elapsed_time_in_step
        type: u4
      - id: pl1_status_parallel
        type: u1
      - id: pl1_sub_status_parallel
        type: u1
      - id: pl1_sub_status_value
        type: u1
      - id: pl1_labonchip_status_address
        type: str
        size: 4
        encoding: ASCII
      - id: pl1_exp_protocol_address_1
        type: str
        size: 4
        encoding: ASCII
      - id: pl1_exp_protocol_address_2
        type: str
        size: 4
        encoding: ASCII
      - id: pl1_exp_protocol_address_3
        type: str
        size: 4
        encoding: ASCII
      - id: pl1_exp_protocol_address_4
        type: str
        size: 4
        encoding: ASCII
      - id: pl1_exp_protocol_address_5
        type: str
        size: 4
        encoding: ASCII
      - id: pl1_exp_protocol_address_6
        type: str
        size: 4
        encoding: ASCII
      - id: memory_errors
        type: u2
      - id: memory_event_start_free_address
        type: str
        size: 4
        encoding: ASCII
      - id: memory_event_end_free_address
        type: str
        size: 4
        encoding: ASCII
      - id: memory_sensors_start_free_address
        type: str
        size: 4
        encoding: ASCII
      - id: memory_sensors_end_free_address
        type: str
        size: 4
        encoding: ASCII
      - id: memory_marie_start_free_address
        type: str
        size: 4
        encoding: ASCII
      - id: memory_marie_end_free_address
        type: str
        size: 4
        encoding: ASCII
      - id: pl1_last_beacon_address
        type: str
        size: 4
        encoding: ASCII
      - id: pl1_experiment_data
        type: str
        size: 4
        encoding: ASCII
      - id: crc
        type: u4
 
  beacon_1:
   seq:
      - id: beacon_header
        type: str
        encoding: ASCII
        size: 3
      - id: radio_idx
        type: u2
      - id: radio_ack_index
        type: u2
      - id: type
        type: u1
      - id: size
        type: u1
      - id: sub_type
        type: u1
      - id: total_mission_minutes
        type: u4
      - id: status
        type: u1
      - id: mission_phase
        type: u1
      - id: time_now
        type: u4
      - id: external_rtc
        type: u4
      - id: watchdog_interval
        type: str
        encoding: ASCII
        size: 3
      - id: telemetry_last_update_external_rtc_unixtime
        type: u4
      - id: radio_last_time_rx_ground
        type: u4
      - id: radio_ack_packet_idx
        type: u2
      - id: radio_temperature
        type: u1
      - id: radio_rssi
        type: u1
      - id: radio_amateur_ham
        type: u1
      - id: temperature_mcu
        type: s1
      - id: temperature_mcu_max
        type: u1
      - id: temperature_mcu_min
        type: s1
      - id: temperature_fpga
        type: s1
      - id: temperature_fpga_max
        type: s1
      - id: temperature_fpga_min
        type: s1
      - id: temperature_additional_data
        type: str
        encoding: ASCII
        size: 2
      - id: magnetometer_x
        type: s2
      - id: magnetometer_y
        type: s2
      - id: magnetometer_z
        type: s2
      - id: giroscope_x
        type: s2
      - id: giroscope_y
        type: s2
      - id: giroscope_z
        type: s2
      - id: abacus_current
        type: u2
      - id: abacus_current_max
        type: s2
      - id:  abacus_current_min
        type: s2
      - id:  abacus_current_avg
        type: u2
      - id: memory_errors
        type: u2
      - id: eps_battery_voltage
        type: u2
      - id: eps_battery_discharge_current
        type: u2
      - id: eps_battery_temperature
        type: u1
      - id: eps_pbus_current
        type: u1
      - id: pl1_experiment_current
        type: u1
      - id: pl1_current_step
        type: u1
      - id: kayser_i2c_bus
        type: u1
      - id: kayser_sensor_status_bus0
        type: u1
      - id: kayser_sensor_status_bus1
        type: u1
      - id: kayser_radfet_led_en
        type: u1
      - id: kayser_wet_sensor_en_sel
        type: u1
      - id: kayser_pump_en
        type: u1
      - id: wet_sensor_1
        type: u2
      - id: wet_sensor_2
        type: u2
      - id: kayser_temperature_1
        type: s1
      - id: kayser_temperature_2
        type: s1
      - id: kayser_temperature_max
        type: s1  
      - id: kayser_temperature_min
        type: s1
      - id: kayser_pressure
        type: u2
      - id: kayser_pressure_max
        type: u2
      - id: kayser_pressure_min
        type: u2
      - id: kayser_pressure_status
        type: str
        encoding: ASCII
        size: 1
      - id: kayser_pressure_temperature
        type: u1
      - id: kayser_luminosity
        type: u2
      - id: kayser_radfet_1
        type: u2
      - id: kayser_radfet_2
        type: u2
      - id: pl1_last_beacon_address
        type: str
        encoding: ASCII
        size: 4
      - id: pl1_ecperiment_data
        type: str
        encoding: ASCII
        size: 128
      - id: crc
        type: u4
        
  beacon_2:
   seq:
      - id: beacon_header
        type: str
        encoding: ASCII
        size: 3
      - id: radio_idx
        type: u2
      - id: radio_ack_index
        type: u2
      - id: type
        type: u1
      - id: size
        type: u1
      - id: sub_type
        type: u1
      - id: total_mission_minutes
        type: u4
      - id: status
        type: u1
      - id: mission_phase
        type: u1
      - id: time_now
        type: u4
      - id: eps_battery_voltage
        type: u2
      - id: eps_battery_voltage_max
        type: u2
      - id: eps_battery_voltage_min
        type: u2
      - id: eps_battery_discharge_current
        type: u2
      - id: eps_battery_discharge_current_max
        type: u2
      - id: eps_battery_discharge_current_min
        type: u2
      - id: eps_battery_charge_current
        type: u1
      - id: eps_battery_charge_current_max
        type: u1
      - id: eps_battery_charge_current_min
        type: u1
      - id: eps_battery_temperature
        type: u1
      - id: eps_battery_temperature_max
        type: u1
      - id: eps_battery_temperature_min
        type: u1
      - id: eps_pv0_current
        type: u1
      - id: eps_pv0_current_max
        type: u1
      - id: eps_pv0_current_min
        type: u1
      - id: eps_pv1_current
        type: u1
      - id: eps_pv1_current_max
        type: u1
      - id: eps_pv1_current_min
        type: u1
      - id: eps_3v3_current
        type: u1
      - id: eps_3v3_current_max
        type: u1
      - id: eps_3v3_current_min
        type: u1
      - id: eps_5v_current
        type: u1
      - id: eps_5v_current_max
        type: u1
      - id: eps_5v_current_min
        type: u1
      - id: eps_pbus_current
        type: u1
      - id: eps_pbus_current_max
        type: u1
      - id: eps_pbus_current_min
        type: u1
      - id: eps_active_sensor
        type: u2
      - id: pl2_start_free_address
        type: str
        encoding: ASCII
        size: 4
      - id: pl2_end_free_address
        type: str
        encoding: ASCII
        size: 4
      - id: pl2_active_flag_status
        type: u2
      - id: pl2_mcu_protected_counts
        type: u2
      - id: pl2_mcu_external_counts
        type: u2
      - id: pl2_imu_protected_counts
        type: u2
      - id: pl2_imu_protected_interval
        type: u2
      - id: pl2_imu_external_counts
        type: u2
      - id: pl2_imu_external_interval
        type: u2
      - id: amature_packets
        type: str
        encoding: ASCII
        size: 2
      - id: amature_tx_packets
        type: str
        encoding: ASCII
        size: 2
      - id: last_time_radio_ham_rx
        type: str
        encoding: ASCII
        size: 4
      - id: last_time_radio_ham_tx
        type: str
        encoding: ASCII
        size: 4
      - id: radio_ham_call_sign
        type: str
        encoding: ASCII
        size: 12
      - id: event_counter
        type: u2
      - id: pl2_last_beacon_address
        type: str
        encoding: ASCII
        size: 4
      - id: pl2_ecperiment_data
        type: str
        encoding: ASCII
        size: 128
      - id: crc
        type: u4