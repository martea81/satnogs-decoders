---
meta:
  id: stecco
  endian: le
doc: |
  :field init: ax25_frame.ax25_header.init
  :field callsign: ax25_frame.ax25_header.src_callsign.callsign
  :field dest_callsign_callsign: ax25_frame.ax25_header.dest_callsign.callsign
  :field ctrl_field: ax25_frame.ax25_header.ctrl_field
  :field pid: ax25_frame.ax25_header.pid
  :field stecco_phoebe_mcu: ax25_frame.ax25_data_field.stecco_phoebe_mcu
  :field pck_idx: ax25_frame.ax25_data_field.pck_idx
  :field last_ack_index: ax25_frame.ax25_data_field.last_ack_index
  :field msg_type: ax25_frame.ax25_data_field.msg_type
  :field msg_syze: ax25_frame.ax25_data_field.msg_syze
  :field status_event: ax25_frame.ax25_data_field.status_event
  :field watch_doginterval_binary: ax25_frame.ax25_data_field.watch_doginterval_binary
  :field rx_mode: ax25_frame.ax25_data_field.rx_mode
  :field ax25_mode_interval: ax25_frame.ax25_data_field.ax25_mode_interval
  :field fec_mode_interval: ax25_frame.ax25_data_field.fec_mode_interval
  :field auto_rx_mode_switching: ax25_frame.ax25_data_field.auto_rx_mode_switching
  :field ignore_crc: ax25_frame.ax25_data_field.ignore_crc
  :field fpga_on_boot: ax25_frame.ax25_data_field.fpga_on_boot
  :field radio_automatic_beacon_interval: ax25_frame.ax25_data_field.radio_automatic_beacon_interval
  :field radio_minimum_interval: ax25_frame.ax25_data_field.radio_minimum_interval
  :field radio_data_maximum_size: ax25_frame.ax25_data_field.radio_data_maximum_size
  :field telemetry_read_interval: ax25_frame.ax25_data_field.telemetry_read_interval
  :field telemetry_save_interval: ax25_frame.ax25_data_field.telemetry_save_interval
  :field mcu_radio_current: ax25_frame.ax25_data_field.mcu_radio_current
  :field fpga_current: ax25_frame.ax25_data_field.fpga_current
  :field rtc_backup_voltage: ax25_frame.ax25_data_field.rtc_backup_voltage
  :field mcu_raw_internal_temperature: ax25_frame.ax25_data_field.mcu_raw_internal_temperature
  :field reserved_check_code: ax25_frame.ax25_data_field.reserved_check_code
  :field number_reboots: ax25_frame.ax25_data_field.number_reboots
  :field number_hard_resets: ax25_frame.ax25_data_field.number_hard_resets
  :field number_fpga_power_cycles: ax25_frame.ax25_data_field.number_fpga_power_cycles
  :field last_time_fpga_power_cycle: ax25_frame.ax25_data_field.last_time_fpga_power_cycle
  :field time_boot: ax25_frame.ax25_data_field.time_boot
  :field errors: ax25_frame.ax25_data_field.errors
  :field last_task: ax25_frame.ax25_data_field.last_task
  :field last_time_rx_mode_fec: ax25_frame.ax25_data_field.last_time_rx_mode_fec
  :field last_time_rx_mode_ax25: ax25_frame.ax25_data_field.last_time_rx_mode_ax25
  :field radio_pkt_index: ax25_frame.ax25_data_field.radio_pkt_index
  :field total_tx_packets: ax25_frame.ax25_data_field.total_tx_packets
  :field radio_ack_pkt_idx: ax25_frame.ax25_data_field.radio_ack_pkt_idx
  :field last_time_rx_ground: ax25_frame.ax25_data_field.last_time_rx_ground
  :field last_time_mcu_command: ax25_frame.ax25_data_field.last_time_mcu_command
  :field last_time_mcu_tx: ax25_frame.ax25_data_field.last_time_mcu_tx
  :field last_time_beacon: ax25_frame.ax25_data_field.last_time_beacon
  :field radio_ackpkt_idx: ax25_frame.ax25_data_field.radio_ackpkt_idx
  :field fpga_rx_packets: ax25_frame.ax25_data_field.fpga_rx_packets
  :field total_rx_packets: ax25_frame.ax25_data_field.total_rx_packets
  :field last_time_radio_reboot: ax25_frame.ax25_data_field.last_time_radio_reboot
  :field telemetry_last_time_read: ax25_frame.ax25_data_field.telemetry_last_time_read
  :field last_time_low_power_mode: ax25_frame.ax25_data_field.last_time_low_power_mode
  :field last_time_normal_mode: ax25_frame.ax25_data_field.last_time_normal_mode
  :field t_calibration_1: ax25_frame.ax25_data_field.t_calibration_1
  :field t_calibration_2: ax25_frame.ax25_data_field.t_calibration_2
  :field mcu_avg_temperature: ax25_frame.ax25_data_field.mcu_avg_temperature
  :field time_event: ax25_frame.ax25_data_field.time_event
  :field event_number: ax25_frame.ax25_data_field.event_number
  :field event_type: ax25_frame.ax25_data_field.event_type
  :field statusevent: ax25_frame.ax25_data_field.statusevent
  :field beacon_message: ax25_frame.ax25_data_field.beacon_message
  :field status_byte: ax25_frame.ax25_data_field.status_byte
  :field call_sign_source: ax25_frame.ax25_data_field.call_sign_source
  :field call_sign_dest: ax25_frame.ax25_data_field.call_sign_dest
  :field up_time: ax25_frame.ax25_data_field.up_time
  :field rtc_seconds: ax25_frame.ax25_data_field.rtc_seconds
  :field rtc_minutes: ax25_frame.ax25_data_field.rtc_minutes
  :field rtc_hours: ax25_frame.ax25_data_field.rtc_hours
  :field rtc_weekday: ax25_frame.ax25_data_field.rtc_weekday
  :field rtc_date_day: ax25_frame.ax25_data_field.rtc_date_day
  :field rtc_date_month: ax25_frame.ax25_data_field.rtc_date_month
  :field rtc_date_year: ax25_frame.ax25_data_field.rtc_date_year
  :field address_telemetry_data_memory_1: ax25_frame.ax25_data_field.address_telemetry_data_memory_1
  :field address_telemetry_data_memory_2: ax25_frame.ax25_data_field.address_telemetry_data_memory_2
  :field address_telemetry_data_memory_3: ax25_frame.ax25_data_field.address_telemetry_data_memory_3
  :field address_event_data_memory_1: ax25_frame.ax25_data_field.address_event_data_memory_1
  :field address_event_data_memory_2: ax25_frame.ax25_data_field.address_event_data_memory_2
  :field address_event_data_memory_3: ax25_frame.ax25_data_field.address_event_data_memory_3
  :field eps_voltage: ax25_frame.ax25_data_field.eps_voltage
  :field eps_data_d1editfield: ax25_frame.ax25_data_field.eps_data_d1editfield
  :field eps_data_d2editfield: ax25_frame.ax25_data_field.eps_data_d2editfield
  :field eps_data_d3editfield: ax25_frame.ax25_data_field.eps_data_d3editfield
  :field eps_data_d4editfield: ax25_frame.ax25_data_field.eps_data_d4editfield
  :field eps_data_d5editfield: ax25_frame.ax25_data_field.eps_data_d5editfield
  :field temperature_sensor: ax25_frame.ax25_data_field.temperature_sensor
  :field temperature_sensor2: ax25_frame.ax25_data_field.temperature_sensor2
  :field radio_ham_events: ax25_frame.ax25_data_field.radio_ham_events
  :field telemetry_events: ax25_frame.ax25_data_field.telemetry_events
  :field configuration_events: ax25_frame.ax25_data_field.configuration_events
  :field seu_events: ax25_frame.ax25_data_field.seu_events
  :field cram_seu_events: ax25_frame.ax25_data_field.cram_seu_events
  :field wdt_events: ax25_frame.ax25_data_field.wdt_events
  :field fpga_sw_on_events: ax25_frame.ax25_data_field.fpga_sw_on_events
  :field radio_bus_injection_events: ax25_frame.ax25_data_field.radio_bus_injection_events
  :field magnetometer_x_1: ax25_frame.ax25_data_field.magnetometer_x_1
  :field magnetometer_y_1: ax25_frame.ax25_data_field.magnetometer_y_1
  :field magnetometer_z_1: ax25_frame.ax25_data_field.magnetometer_z_1
  :field gyroscope_x_1: ax25_frame.ax25_data_field.gyroscope_x_1
  :field gyroscope_y_1: ax25_frame.ax25_data_field.gyroscope_y_1
  :field gyroscope_z_1: ax25_frame.ax25_data_field.gyroscope_z_1
  :field magnetometer_x_2: ax25_frame.ax25_data_field.magnetometer_x_2
  :field magnetometer_y_2: ax25_frame.ax25_data_field.magnetometer_y_2
  :field magnetometer_z_2: ax25_frame.ax25_data_field.magnetometer_z_2
  :field gyroscope_x_2: ax25_frame.ax25_data_field.gyroscope_x_2
  :field gyroscope_y_2: ax25_frame.ax25_data_field.gyroscope_y_2
  :field gyroscope_z_2: ax25_frame.ax25_data_field.gyroscope_z_2
  :field magnetometer_x_3: ax25_frame.ax25_data_field.magnetometer_x_3
  :field magnetometer_y_3: ax25_frame.ax25_data_field.magnetometer_y_3
  :field magnetometer_z_3: ax25_frame.ax25_data_field.magnetometer_z_3
  :field gyroscope_x_3: ax25_frame.ax25_data_field.gyroscope_x_3
  :field gyroscope_y_3: ax25_frame.ax25_data_field.gyroscope_y_3
  :field gyroscope_z_3: ax25_frame.ax25_data_field.gyroscope_z_3
  :field magnetometer_x_4: ax25_frame.ax25_data_field.magnetometer_x_4
  :field magnetometer_y_4: ax25_frame.ax25_data_field.magnetometer_y_4
  :field magnetometer_z_4: ax25_frame.ax25_data_field.magnetometer_z_4
  :field gyroscope_x_4: ax25_frame.ax25_data_field.gyroscope_x_4
  :field gyroscope_y_4: ax25_frame.ax25_data_field.gyroscope_y_4
  :field gyroscope_z_4: ax25_frame.ax25_data_field.gyroscope_z_4
  :field magnetometer_x_5: ax25_frame.ax25_data_field.magnetometer_x_5
  :field magnetometer_y_5: ax25_frame.ax25_data_field.magnetometer_y_5
  :field magnetometer_z_5: ax25_frame.ax25_data_field.magnetometer_z_5
  :field gyroscope_x_5: ax25_frame.ax25_data_field.gyroscope_x_5
  :field gyroscope_y_5: ax25_frame.ax25_data_field.gyroscope_y_5
  :field gyroscope_z_5: ax25_frame.ax25_data_field.gyroscope_z_5
  :field magnetometer_x_6: ax25_frame.ax25_data_field.magnetometer_x_6
  :field magnetometer_y_6: ax25_frame.ax25_data_field.magnetometer_y_6
  :field magnetometer_z_6: ax25_frame.ax25_data_field.magnetometer_z_6
  :field gyroscope_x_7: ax25_frame.ax25_data_field.gyroscope_x_7
  :field gyroscope_y_7: ax25_frame.ax25_data_field.gyroscope_y_7
  :field gyroscope_z_7: ax25_frame.ax25_data_field.gyroscope_z_7
  :field magnetometer_x_8: ax25_frame.ax25_data_field.magnetometer_x_8
  :field magnetometer_y_8: ax25_frame.ax25_data_field.magnetometer_y_8
  :field magnetometer_z_8: ax25_frame.ax25_data_field.magnetometer_z_8
  :field gyroscope_x_8: ax25_frame.ax25_data_field.gyroscope_x_8
  :field gyroscope_y_8: ax25_frame.ax25_data_field.gyroscope_y_8
  :field gyroscope_z_8: ax25_frame.ax25_data_field.gyroscope_z_8
  :field magnetometer_x_9: ax25_frame.ax25_data_field.magnetometer_x_9
  :field magnetometer_y_9: ax25_frame.ax25_data_field.magnetometer_y_9
  :field magnetometer_z_9: ax25_frame.ax25_data_field.magnetometer_z_9
  :field gyroscope_x_9: ax25_frame.ax25_data_field.gyroscope_x_9
  :field gyroscope_y_9: ax25_frame.ax25_data_field.gyroscope_y_9
  :field gyroscope_z_9: ax25_frame.ax25_data_field.gyroscope_z_9
  :field magnetometer_x_10: ax25_frame.ax25_data_field.magnetometer_x_10
  :field magnetometer_y_10: ax25_frame.ax25_data_field.magnetometer_y_10
  :field magnetometer_z_10: ax25_frame.ax25_data_field.magnetometer_z_10
  :field gyroscope_x_10: ax25_frame.ax25_data_field.gyroscope_x_10
  :field gyroscope_y_10: ax25_frame.ax25_data_field.gyroscope_y_10
  :field gyroscope_z_10: ax25_frame.ax25_data_field.gyroscope_z_10
  :field magnetometer_x_11: ax25_frame.ax25_data_field.magnetometer_x_11
  :field magnetometer_y_11: ax25_frame.ax25_data_field.magnetometer_y_11
  :field magnetometer_z_11: ax25_frame.ax25_data_field.magnetometer_z_11
  :field gyroscope_x_11: ax25_frame.ax25_data_field.gyroscope_x_11
  :field gyroscope_y_11: ax25_frame.ax25_data_field.gyroscope_y_11
  :field gyroscope_z_11: ax25_frame.ax25_data_field.gyroscope_z_11
  :field idx_back: ax25_frame.ax25_data_field.idx_back
  :field beacon_req: ax25_frame.ax25_data_field.beacon_req
  :field length: ax25_frame.ax25_data_field.length
  :field rssi: ax25_frame.ax25_data_field.rssi
  :field rx: ax25_frame.ax25_data_field.rx
  :field tx: ax25_frame.ax25_data_field.tx
  :field rx_err: ax25_frame.ax25_data_field.rx_err
  :field tx_err: ax25_frame.ax25_data_field.tx_err

seq:
  - id: ax25_frame
    type: ax25_frame
    doc-ref: 'https://www.researchgate.net/figure/AX25-data-frame-in-KISS-mode-for-the-requested-file-upload-command_fig3_320035650'
    
types:
  ax25_frame:
    seq:
      - id: ax25_header
        type: ax25_header
      - id: ax25_data_field
        type:
          switch-on: _io.size #ax25_header.ctl & 0x13 # CAPIRE COME DISTINGUERE TRA MCU, FPGA E RADIO
          cases:
            218: mcu_data_field
            184: fpga_data_field
            37: radio_data_field

  ax25_header:
    seq:
      - id: init
        type: u1
      - id: src_callsign
        type: callsign
      - id: dest_callsign
        type: callsign
      - id: ctrl_field
        type: u1
      - id: pid
        type: u2
  callsign:
    seq:
      - id: callsign
        process: ror(1)
        size: 7
  

  mcu_data_field:
    seq:
      - id: stecco_phoebe_mcu
        type: str
        size: 18
        encoding: ASCII
      - id: pck_idx
        type: u2
      - id: last_ack_index
        type: u2
      - id: msg_type
        type: u1
      - id: msg_syze
        type: u1
      - id: status_event
        type: u1
      - id: watch_doginterval_binary
        type: u1
      - id: rx_mode
        type: u1
      - id: ax25_mode_interval
        type: u1
      - id: fec_mode_interval
        type: u1
      - id: auto_rx_mode_switching
        type: u1
      - id: ignore_crc
        type: u1
      - id: fpga_on_boot 
        type: u2
      - id: radio_automatic_beacon_interval
        type: u1
      - id: radio_minimum_interval
        type: u2
      - id: radio_data_maximum_size
        type: u1
      - id: telemetry_read_interval
        type: u1
      - id: telemetry_save_interval
        type: u2
      - id: mcu_radio_current
        type: u2
      - id: fpga_current
        type: u2
      - id: rtc_backup_voltage
        type: u2
      - id: mcu_raw_internal_temperature
        type: u2
      - id: reserved_check_code 
        type: u1
      - id: number_reboots
        type: u2
      - id: number_hard_resets 
        type: u2
      - id: number_fpga_power_cycles
        type: u2
      - id:  last_time_fpga_power_cycle
        type: u4
      - id:  time_boot
        type: u4
      - id: errors
        type: u2
      - id: last_task
        type: u1
      - id: last_time_rx_mode_fec
        type: u4
      - id: last_time_rx_mode_ax25
        type: u4
      - id: radio_pkt_index
        type: u2
      - id: total_tx_packets
        type: u2
      - id:  radio_ack_pkt_idx 
        type: u2
      - id: last_time_rx_ground 
        type: u4
      - id: last_time_mcu_command 
        type: u4
      - id: last_time_mcu_tx
        type: u4
      - id:  last_time_beacon
        type: u4
      - id: radio_ackpkt_idx 
        type: u2
      - id: fpga_rx_packets
        type: u2
      - id:  total_rx_packets
        type: u2
      - id:  last_time_radio_reboot
        type: u4
      - id: telemetry_last_time_read
        type: u4
      - id: last_time_low_power_mode
        type: u4
      - id: last_time_normal_mode
        type: u4
      - id: t_calibration_1
        type: u2
      - id: t_calibration_2
        type: u2
      - id: mcu_avg_temperature
        type: f4
      - id:  time_event 
        type: u4
      - id: event_number 
        type: u2
      - id: event_type 
        type: u1
      - id: statusevent 
        type: u1
      - id: beacon_message
        type: str
        size: 70
        encoding: ASCII
        
  fpga_data_field:
    seq:
      - id: status_byte
        type: u1
      - id: call_sign_source
        type: str
        size: 6
        encoding: ASCII
      - id:  call_sign_dest
        type: str
        size: 6
        encoding: ASCII
      - id: up_time
        type: u4
      - id: rtc_seconds
        type: u1
      - id: rtc_minutes
        type: u1
      - id: rtc_hours
        type: u1
      - id: rtc_weekday
        type: u1
      - id: rtc_date_day
        type: u1
      - id: rtc_date_month
        type: u1
      - id: rtc_date_year
        type: u1
      - id: auto_rx_mode_switching
        type: u1
      - id: address_telemetry_data_memory_1
        type: u1
      - id: address_telemetry_data_memory_2 
        type: u1
      - id: address_telemetry_data_memory_3
        type: u1
      - id: address_event_data_memory_1
        type: u1
      - id: address_event_data_memory_2
        type: u1
      - id: address_event_data_memory_3
        type: u1
      - id: eps_voltage
        type: u1
      - id: eps_data_d1editfield
        type: u1
      - id: eps_data_d2editfield
        type: u1
      - id: eps_data_d3editfield
        type: u1
      - id: eps_data_d4editfield
        type: u1
      - id: eps_data_d5editfield 
        type: u1
      - id: temperature_sensor
        type: s2
      - id: temperature_sensor2 
        type: s2
      - id: radio_ham_events
        type: u1
      - id:  telemetry_events
        type: u1
      - id:  configuration_events
        type: u1
      - id: seu_events
        type: u1
      - id: cram_seu_events
        type: u1
      - id: wdt_events
        type: u1
      - id: fpga_sw_on_events
        type: u1
      - id: radio_bus_injection_events
        type: u1
      - id: magnetometer_x_1
        type: s2
      - id: magnetometer_y_1
        type: s2
      - id: magnetometer_z_1
        type: s2
      - id: gyroscope_x_1
        type: s2
      - id: gyroscope_y_1
        type: s2
      - id: gyroscope_z_1
        type: s2
      - id: magnetometer_x_2
        type: s2
      - id: magnetometer_y_2
        type: s2
      - id: magnetometer_z_2
        type: s2
      - id: gyroscope_x_2
        type: s2
      - id: gyroscope_y_2
        type: s2
      - id: gyroscope_z_2
        type: s2
      - id: magnetometer_x_3
        type: s2
      - id: magnetometer_y_3
        type: s2
      - id: magnetometer_z_3
        type: s2
      - id: gyroscope_x_3
        type: s2
      - id: gyroscope_y_3
        type: s2
      - id: gyroscope_z_3
        type: s2
      - id: magnetometer_x_4
        type: s2
      - id: magnetometer_y_4
        type: s2
      - id: magnetometer_z_4
        type: s2
      - id: gyroscope_x_4
        type: s2
      - id: gyroscope_y_4
        type: s2
      - id: gyroscope_z_4
        type: s2
      - id: magnetometer_x_5
        type: s2
      - id: magnetometer_y_5
        type: s2
      - id: magnetometer_z_5 
        type: s2
      - id: gyroscope_x_5
        type: s2
      - id: gyroscope_y_5
        type: s2
      - id: gyroscope_z_5
        type: s2
      - id: magnetometer_x_6
        type: s2
      - id: magnetometer_y_6 
        type: s2
      - id: magnetometer_z_6
        type: s2
      - id: gyroscope_x_7
        type: s2
      - id: gyroscope_y_7
        type: s2
      - id: gyroscope_z_7
        type: s2
      - id: magnetometer_x_8
        type: s2
      - id: magnetometer_y_8 
        type: s2
      - id: magnetometer_z_8
        type: s2
      - id: gyroscope_x_8
        type: s2
      - id: gyroscope_y_8
        type: s2
      - id: gyroscope_z_8
        type: s2
      - id: magnetometer_x_9
        type: s2
      - id: magnetometer_y_9
        type: s2
      - id: magnetometer_z_9 
        type: s2
      - id: gyroscope_x_9
        type: s2
      - id: gyroscope_y_9
        type: s2
      - id: gyroscope_z_9
        type: s2
      - id: magnetometer_x_10
        type: s2
      - id: magnetometer_y_10 
        type: s2
      - id: magnetometer_z_10
        type: s2
      - id: gyroscope_x_10
        type: s2
      - id: gyroscope_y_10
        type: s2
      - id: gyroscope_z_10
        type: s2
      - id: magnetometer_x_11
        type: s2
      - id: magnetometer_y_11 
        type: s2
      - id: magnetometer_z_11
        type: s2
      - id: gyroscope_x_11
        type: s2
      - id: gyroscope_y_11
        type: s2
      - id: gyroscope_z_11
        type: s2

  radio_data_field:
    seq:
      - id: idx_back
        type: str
        size: 2
        encoding: ASCII
        type: u1
      - id: beacon_req
        type: u1
      - id: length
        type: u1
      - id: rssi
        type: u1
      - id: rx
        type: u4
      - id: tx
        type: u4
      - id: rx_err
        type: u4
      - id: tx_err
        type: u4